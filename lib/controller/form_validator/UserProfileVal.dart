import 'package:flutter/material.dart';
import 'package:mypkg/controller/Mixin.dart';

class UserProfileVal with Mixin {
  static const int PHONE_LIMIT = 6;

  isEmpty(TextEditingController tf, arg, Color bgColor) {
    if (tf.text.isEmpty) {
      showToast(txtColor: Colors.white, bgColor: bgColor, msg: arg);
      return true;
    }
    return false;
  }

  isFullName(String name, Color bgColor) {
    try {
      final isOk = (name.trim().contains(" ")) ? true : false;
      if (!isOk) {
        showToast(
            txtColor: Colors.white, bgColor: bgColor, msg: "Invalid full name");
      }
      return isOk;
    } catch (e) {
      return false;
    }
  }

  isFNameOK(TextEditingController tf, Color bgColor) {
    if (tf.text.length == 0) {
      showToast(
          txtColor: Colors.white, bgColor: bgColor, msg: "Invalid first name");
      return false;
    }
    return true;
  }

  isLNameOK(TextEditingController tf, Color bgColor) {
    if (tf.text.length == 0) {
      showToast(
          txtColor: Colors.white, bgColor: bgColor, msg: "Invalid last name");
      return false;
    }
    return true;
  }

  isEmailOK(TextEditingController tf, String msg, Color bgColor) {
    if (!RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(tf.text.trim())) {
      showToast(txtColor: Colors.white, bgColor: bgColor, msg: msg);
      return false;
    } else if (tf.text.trim().contains("@help.mortgage-magic.co.uk")) {
      showToast(
          txtColor: Colors.white,
          bgColor: bgColor,
          msg: "Email address does not exists");
      return false;
    }
    return true;
  }

  isPhoneOK(TextEditingController tf, Color bgColor) {
    if (tf.text.length < PHONE_LIMIT) {
      showToast(
          txtColor: Colors.white,
          bgColor: bgColor,
          msg: "Invalid Mobile Number");
      return false;
    }
    return true;
  }

  isPwdOK(TextEditingController tf, Color bgColor) {
    if (tf.text.length < 3) {
      showToast(
          txtColor: Colors.white,
          bgColor: bgColor,
          msg: "Password should be greater by 4 characters");
      return false;
    }
    return true;
  }

  isComNameOK(TextEditingController tf, Color bgColor) {
    if (tf.text.length < 6) {
      showToast(
          txtColor: Colors.white,
          bgColor: bgColor,
          msg: "Invalid Company Name");
      return false;
    }
    return true;
  }

  isDOBOK(str, Color bgColor) {
    if (str == '') {
      showToast(
          txtColor: Colors.white,
          bgColor: bgColor,
          msg: "Invalid Date of Birth");
      return false;
    }
    return true;
  }
}
