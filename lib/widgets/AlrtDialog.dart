import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mypkg/controller/Mixin.dart';

class AlrtDialog extends StatefulWidget {
  final int which;
  final String msg;
  final Color bgColor;
  Color txtColor;
  AlrtDialog({
    Key key,
    @required this.which,
    @required this.msg,
    @required this.bgColor,
    this.txtColor = Colors.white,
  }) : super(key: key);
  @override
  State createState() => _AlertDlgState();
}

class _AlertDlgState extends State<AlrtDialog> with Mixin {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: widget.bgColor,
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Container(
          child: ListView(
            shrinkWrap: true,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(20),
                child: Text(
                  widget.msg,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: widget.txtColor,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    height: 1.5,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                    left: getWP(context, 25), right: getWP(context, 25)),
                child: MaterialButton(
                  onPressed: () {
                    Get.back();
                  },
                  child: Text(
                    "Ok",
                    style: TextStyle(
                      color: widget.txtColor,
                      fontSize: 20,
                    ),
                  ),
                  color: Colors.grey,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
